\babel@toc {english}{}\relax 
\beamer@sectionintoc {1}{面向对象}{2}{0}{1}
\beamer@sectionintoc {2}{函数式特性}{19}{0}{2}
\beamer@subsectionintoc {2}{1}{Closure}{29}{0}{2}
\beamer@subsectionintoc {2}{2}{Partial Function}{31}{0}{2}
\beamer@subsectionintoc {2}{3}{List Comprehension}{33}{0}{2}
\beamer@subsectionintoc {2}{4}{Map-Reduce}{35}{0}{2}
\beamer@sectionintoc {3}{一点更多的数据结构}{37}{0}{3}
\beamer@subsectionintoc {3}{1}{元组}{38}{0}{3}
\beamer@subsectionintoc {3}{2}{字典}{44}{0}{3}
\beamer@subsectionintoc {3}{3}{集合}{52}{0}{3}
