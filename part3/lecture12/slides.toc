\babel@toc {english}{}\relax 
\beamer@sectionintoc {1}{A Minimal Application}{3}{0}{1}
\beamer@sectionintoc {2}{Routing}{10}{0}{2}
\beamer@sectionintoc {3}{Rendering Templates}{16}{0}{3}
\beamer@sectionintoc {4}{Accessing Request Data}{24}{0}{4}
\beamer@sectionintoc {5}{Redirects and Errors}{27}{0}{5}
\beamer@sectionintoc {6}{Sessions}{31}{0}{6}
\beamer@sectionintoc {7}{A Demo}{35}{0}{7}
